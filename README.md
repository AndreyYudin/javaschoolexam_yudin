# README #

This is a repo with T-Systems Java School preliminary examination tasks.
Code points where you solution is to be located are marked with TODOs.

The solution is to be written using Java 1.8 only, external libraries are forbidden. 
You can add dependencies with scope "test" if it's needed to write new unit-tests.

The exam includes 3 tasks to be done: [Calculator](/tasks/Calculator.md), [Pyramid](/tasks/Pyramid.md), and 
[Subsequence](/tasks/Subsequence.md)

### Result ###

* Author name : {Andrey Yudin}
* Codeship : {[ ![Codeship Status for AndreyYudin/javaschoolexam_yudin](https://app.codeship.com/projects/8f0efef0-808c-0136-632d-7275346b3fc4/status?branch=master)](https://app.codeship.com/projects/301565)}

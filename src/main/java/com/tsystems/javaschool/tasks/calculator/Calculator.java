package com.tsystems.javaschool.tasks.calculator;

import java.util.LinkedList;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        // TODO: Implement the logic here
        //chek of the validity of the expression
        if (statement == null || statement.isEmpty() || statement.contains(","))
            return null;
        int count = 0;
        for (int i = 0; i < statement.length(); i++) {

            if (isOperator(statement.charAt(i)) || statement.charAt(i) == '.') {
                if (i + 1 <= statement.length() && (isOperator(statement.charAt(i + 1)) || statement.charAt(i + 1) == '.')) {
                    return null;
                }
            }
            if (statement.charAt(i) == '(') count++;
            if (statement.charAt(i) == ')') count--;
        }
        if (count != 0)
            return null;

        LinkedList<Double> st = new LinkedList();
        LinkedList<Character> listOperation = new LinkedList<Character>();
        for (int i = 0; i < statement.length(); i++) {
            char ch = statement.charAt(i);
            if (ch == '(')
                listOperation.add(ch);
            else if (ch == ')') {
                while (listOperation.getLast() != '(')
                    doOperation(st, listOperation.removeLast());
                listOperation.removeLast();
            } else if (isOperator(ch)) {
                while (!listOperation.isEmpty() && priorityOperation(listOperation.getLast()) >= priorityOperation(ch))
                    doOperation(st, listOperation.removeLast());
                listOperation.add(ch);

            } else {
                String operand = "";
                while (i < statement.length() && (Character.isDigit(statement.charAt(i)) || statement.charAt(i) == '.'))
                    operand += statement.charAt(i++);
                --i;
                st.add(Double.parseDouble(operand));
            }
        }
        while (!listOperation.isEmpty())
            doOperation(st, listOperation.removeLast());
        if (st.get(0).isInfinite()) return null;
        return formatResult(st.get(0));
    }

    private static int priorityOperation(char operation) {
        switch (operation) {
            case '+':
            case '-':
                return 1;
            case '*':
            case '/':
                return 2;
            default:
                return -1;
        }
    }

    private static void doOperation(LinkedList<Double> st, char operation) {
        Double y = st.removeLast();
        Double x = st.removeLast();
        switch (operation) {
            case '+':
                st.add(x + y);
                break;
            case '-':
                st.add(x - y);
                break;
            case '*':
                st.add(x * y);
                break;
            case '/':
                st.add(x / y);
                break;
        }
    }

    private static boolean isOperator(char op) {
        return op == '+' || op == '-' || op == '*' || op == '/';
    }

    private static String formatResult(double value) {
        return String.format("%.4f", value).replaceAll("0+$", "").replaceAll("\\.$", "");
    }
}

package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public static int[][] buildPyramid(List<Integer> inputNumbers) throws CannotBuildPyramidException{
        // TODO: Implement the logic here
        if (inputNumbers.size() > 200 || inputNumbers.contains(null)) {
            throw new CannotBuildPyramidException();
        }
        int height = 0;
        int size = inputNumbers.size();
        for (int i = 0; i < inputNumbers.size(); i++) {
            size -= i;
            if (size > 0)
                height++;
        }
        Collections.sort(inputNumbers);
        int[][] pyramid = new int[height][height * 2 - 1];
        int indexInputNumber = 0;
        for (int i = 0; i < height; i++) {
            int s = height - 1 - i;
            for (int j = 0; j <= i; j++) {
                pyramid[i][s] = inputNumbers.get(indexInputNumber);
                s += 2;
                indexInputNumber++;
            }
        }
        return pyramid;
    }
}
